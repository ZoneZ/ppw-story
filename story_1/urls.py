from django.conf.urls import url

from .views import index, friend_profile

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^friend_profile/$', friend_profile, name='friend_profile')
]