from datetime import datetime, date

current_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 8, 21)


def calculate_age(birth_year):
    return current_year - birth_year if birth_year <= current_year else 0