from datetime import date

from django.shortcuts import render

from .utils import birth_date, calculate_age

response = {}


def index(request):
    return render(request, 'index.html', {'name': 'Farhan Azyumardhi Azmi',
                                          'npm': 170697934,
                                          'age': calculate_age(birth_date.year),
                                          'kuliah': 'University of Indonesia'})


def friend_profile(request):
    response['nuga_name'] = 'Muhammad Ardivan Nugroho Satrio'
    response['nuga_npm'] = 1706025371
    response['nuga_age'] = calculate_age(date(1999, 8, 14).year)
    response['nuga_hobby'] = 'make jokes'
    response['sayid_name'] = 'Sayid Abyan Rizal Shiddiq'
    response['sayid_npm'] = 1706022445
    response['sayid_age'] = calculate_age(date(1999, 7, 21).year)
    response['sayid_hobby'] = 'watch anime'
    return render(request, 'friends_profile.html', response)
