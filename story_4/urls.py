from django.conf.urls import url

from .views import about, blog, contact, guest_book, index, lpj_page, rumbuk_page

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/$', about, name='about'),
    url(r'^blog/$', blog, name='blog'),
    url(r'^contact/$', contact, name='contact'),
    url(r'^guest-book/$', guest_book, name='guest-book'),
    url(r'^project/lpj/', lpj_page, name='lpj'),
    url(r'^project/rumbuk/', rumbuk_page, name='rumbuk'),
]