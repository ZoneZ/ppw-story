from django.shortcuts import render


def index(request):
    html = 'index.html'
    return render(request, html)


def about(request):
    html = 'about.html'
    return render(request, html)


def blog(request):
    html = 'blog.html'
    return render(request, html)


def contact(request):
    html = 'contact.html'
    return render(request, html)


def guest_book(request):
    html = 'guest-book.html'
    return render(request, html)


def lpj_page(request):
    html = 'lpj.html'
    return render(request, html)


def rumbuk_page(request):
    html = 'rumbuk.html'
    return render(request, html)