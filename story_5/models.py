from django.core.validators import MaxLengthValidator
from django.db import models


class JadwalPribadi(models.Model):
    name = models.CharField(default='', max_length=30,
                            validators=[MaxLengthValidator(30)])
    place = models.CharField(default='', max_length=30,
                             validators=[MaxLengthValidator(30)])
    category = models.CharField(null=True, blank=True, default='', max_length=30,
                                validators=[MaxLengthValidator(30)])
    time = models.DateTimeField()

    class Meta:
        verbose_name_plural = 'Jadwal Pribadi'

    def __str__(self):
        return self.name