from django import forms

from .models import JadwalPribadi


class JadwalForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(JadwalForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Nama Kegiatan'
        self.fields['name'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan nama kegiatan'}
        self.fields['name'].error_messages = {'max_length': 'Maksimal karakter adalah 30 karakter.'}

        self.fields['place'].label = 'Tempat Kegiatan'
        self.fields['place'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan tempat kegiatan'}
        self.fields['place'].error_messages = {'max_length': 'Maksimal karakter adalah 30 karakter.'}

        self.fields['category'].label = 'Kategori Kegiatan'
        self.fields['category'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan kategori kegiatan'}
        self.fields['category'].error_messages = {'max_length': 'Maksimal karakter adalah 30 karakter.'}

        self.fields['time'] = forms.DateTimeField(
            widget=forms.DateTimeInput(
                attrs={'class': 'form-control',
                       'type': 'datetime-local'}),
            label='Jadwal Kegiatan',
            input_formats=['%Y-%d-%mT%H:%M'],
            error_messages={'invalid': 'Format tanggal dan waktu tidak valid.'})

    class Meta:
        model = JadwalPribadi
        fields = ['name', 'place', 'category', 'time']