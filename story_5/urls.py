from django.conf.urls import url
from .views import ScheduleCreate, ScheduleList, delete_all_schedules

urlpatterns = [
    url(r'^create/$', ScheduleCreate.as_view(), name='schedule-form'),
    url(r'^list/$', ScheduleList.as_view(), name='schedule-list'),
    url(r'^delete/$', delete_all_schedules, name='schedule-delete')
]