from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, ListView

from .forms import JadwalForm
from .models import JadwalPribadi


class ScheduleCreate(CreateView):
    model = JadwalPribadi
    form_class = JadwalForm
    template_name = 'schedule-form.html'

    def get_success_url(self):
        return reverse('schedule:schedule-list')


class ScheduleList(ListView):
    model = JadwalPribadi
    template_name = 'schedule-list.html'
    context_object_name = 'schedule'
    queryset = JadwalPribadi.objects.all()


def delete_all_schedules(request):
    JadwalPribadi.objects.all().delete()
    return HttpResponseRedirect(reverse('schedule:schedule-list'))